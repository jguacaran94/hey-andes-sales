import { Injectable } from '@angular/core'
import moment from 'moment'
import sales from '../sales.json'
import { Enterprise } from './enterprise'

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {
  public enterprises: Enterprise[]
  public topSale: number
  public topMonth: string
  public currentEnterprise: Enterprise

  constructor() {
    this.enterprises = []
    this.topSale = 0
    this.topMonth = ''
    this.currentEnterprise = {
      nameAgency: '',
      finalPrice: 0,
      commission: 0 * 0.025
    }
  }

  getEnterprises() {
    sales.forEach((sale, index) => {
      this.currentEnterprise = {
        nameAgency: sale.nameAgency,
        finalPrice: sale.finalPrice,
        commission: sale.finalPrice * 0.025
      }
      const checkEnterprise = this.enterprises.find((enterprise) => {
        return enterprise.nameAgency === sale.nameAgency
      })
      if (checkEnterprise) {
        checkEnterprise.finalPrice = checkEnterprise.finalPrice + this.currentEnterprise.finalPrice
      } else {
        this.enterprises.push(this.currentEnterprise)
      }
    })
    return this.enterprises
  }

  getTopSales() {
    return this.topSale = this.enterprises.sort((a, b) => a.finalPrice - b.finalPrice)[4].finalPrice
  }

  getTopMonth() {
    const sortedDates: string[] = []
    sales.find((sale) => {
      sortedDates.push(sale.datePayment)
    })
    sortedDates.sort()
    const parsedMonths: string[] = []
    for (let date in sortedDates) {
      const parsedMonth = moment(sortedDates[date], 'DD-MM-YYYY').format('MMMM')
      parsedMonths.push(parsedMonth)
    }
    let countMonths: any = {}
    parsedMonths.forEach((month: any) => {
      countMonths[month] = (countMonths[month] || 0) + 1
    })
    let sortMonths: any = []
    for (let month in countMonths) {
      sortMonths.push([month, countMonths[month]])
    }
    sortMonths.sort((a: any, b: any) => {
      if (a[1] < b[1]) {
        return -1
      } else if (a[1] > b[1]) {
        return 1
      } else {
        return 0
      }
    })
    return this.topMonth = sortMonths.reverse()[0][0]
  }
}
