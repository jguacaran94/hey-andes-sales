import { Component, OnInit, OnDestroy } from '@angular/core'
import { EnterpriseService } from './enterprise.service'
import { Enterprise } from './enterprise'

@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html',
  styleUrls: ['./enterprises.component.sass']
})
export class EnterprisesComponent {
  public enterprises: Enterprise[]
  public topSale: number
  public topMonth: string

  constructor(
    private readonly enterpriseService: EnterpriseService
  ) {
    this.enterprises = []
    this.topSale = 0
    this.topMonth = ''
  }

  ngOnInit(): void {
    this.enterpriseService.getEnterprises()
    this.enterprises = this.enterpriseService.enterprises
    this.enterpriseService.getTopSales()
    this.topSale = this.enterpriseService.topSale
    this.enterpriseService.getTopMonth()
    this.topMonth = this.enterpriseService.topMonth
  }

  ngOnDestroy(): void {
    this.enterpriseService.enterprises = []
    this.enterprises = []
    this.enterpriseService.topSale = 0
    this.topSale = 0
    this.enterpriseService.topMonth = ''
    this.topMonth = ''
  }

  setCurrentEnterprise(enterprise: any) {
    this.enterpriseService.currentEnterprise = enterprise
  }
}
