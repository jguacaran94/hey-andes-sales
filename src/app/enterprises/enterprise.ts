export class Enterprise {
  constructor(
    public nameAgency: string,
    public finalPrice: number,
    public commission: number
  ) {
    this.nameAgency = ''
    this.finalPrice = 0
    this.commission = this.finalPrice * 0.025
  }
}
