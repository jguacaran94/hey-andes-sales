import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { EnterprisesComponent } from './enterprises/enterprises.component'
import { SalesComponent } from './sales/sales.component'

const routes: Routes = [
  {
    path: 'empresas',
    component: EnterprisesComponent,
  },
  {
    path: 'empresas/:nameAgency',
    component: SalesComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  EnterprisesComponent,
  SalesComponent
]
