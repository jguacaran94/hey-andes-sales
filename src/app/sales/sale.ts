export class Sale {
  constructor(
    public name: string,
    public persons: number,
    public hour: string,
    public finalPrice: number,
    public day: string | undefined
  ) {
    this.name = ''
    this.persons = 0
    this.hour = ''
    this.finalPrice = 0
    this.day = ''
  }
}
