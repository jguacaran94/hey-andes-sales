import { Component, OnInit } from '@angular/core'
import sales from '../sales.json'
import { Sale } from './sale'
import { EnterpriseService } from './../enterprises/enterprise.service'
import { Enterprise } from '../enterprises/enterprise'

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.sass']
})
export class SalesComponent implements OnInit {
  public currentSales: Sale[]
  public currentEnterprise: Enterprise

  constructor(
    private readonly enterpriseService: EnterpriseService
  ) {
    this.currentSales = []
    this.currentEnterprise = this.enterpriseService.currentEnterprise
  }

  ngOnInit(): void {
    this.currentEnterprise = this.enterpriseService.currentEnterprise
    this.getSales()
  }

  getSales() {
    sales.forEach((sale) => {
      if (sale.nameAgency === this.currentEnterprise.nameAgency) {
        this.currentSales.push({
          name: sale.name,
          persons: sale.persons,
          hour: sale.hour,
          finalPrice: sale.finalPrice,
          day: sale.day
        })
      }
    })
    return this.currentSales
  }
}
