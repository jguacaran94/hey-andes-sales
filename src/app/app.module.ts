import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { MatCardModule } from '@angular/material/card'
import { MatTableModule } from '@angular/material/table'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NavbarComponent } from './navbar/navbar.component'
import { EnterprisesComponent } from './enterprises/enterprises.component'
import { SalesComponent } from './sales/sales.component'

@NgModule({
  exports: [
    MatCardModule,
    MatTableModule
  ],
  declarations: [
    AppComponent,
    NavbarComponent,
    EnterprisesComponent,
    SalesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
